inp = open("input.txt", 'r')
out = open("output.txt", 'w')
mass = set() # спейсеры
first = set() # тип А
second = set() # тип Б
third = set() # мусор
dr = set() # повторы
name = '' # имя бактерии
num = int(inp.readline()) # последний номер бактерии в базе
out.write('create or replace function change() returns void as $$\n\tdeclare\n\t\ti integer;\n\t\tbf text;\n\tbegin\n')
for i in inp:
    x = i.strip()
    if x == '':
        if len(mass) > 0:
            r = 0
            for g in dr:
                if g[-6:] != 'AAAAAC' and g[-5:] == 'AAAAC':
                    r = 2
                    break
                elif g[-4:] != 'AAAC' and g[-3:] == 'AAC':
                    r = 1
                    break
                elif g[-5:] != 'AAAAC' and g[-4:] == 'AAAC': # сравнение повтора с шаблоном(оканчивается на AAC - 99.7% B, на AAAAC - 99.8% A)
                    r = 0
                    break
            for g in mass: # распределение по типам
                if r == 0:
                    third.add(g)
                elif r == 1:
                    second.add(g)
                else:
                    first.add(g)
        mass = set()
        dr = set() # обнуление
    elif x.split()[0] == 'Streptococcus': # если бактерия
        if len(name) > 0:
            out.write('\t\tinsert into "BacStamms"' + " values(" + str(num + 1) + ", '" + "a_" + name.lower() + "', '{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}', '{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}', '{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}', '{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}');\n")
            out.write('\t\tinsert into "BacStamms"' + " values(" + str(num + 2) + ", '" + "b_" + name.lower() + "', '{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}', '{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}', '{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}', '{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}');\n")
            out.write('\t\tfor i in select "Id" from "Spacers" where "BacStammId" = (select "Id" from "BacStamms" where "Code"' + " = '" + name.lower() + "' limit 1) loop\n")
            out.write('\t\t\tbf = (select "Code" from "Spacers" where "Id" = i limit 1);\n')
            out.write('\t\t\tif bf = any ')
            out.write("(('{")
            for ind, g in enumerate(first):
                out.write(g + (", " if ind != len(first) - 1 else ''))
            out.write("}')::text[]) then\n")
            out.write('\t\t\t\tupdate "Spacers" set "BacStammId" = ' + str(num + 1) + ' where "Id" = i;\n')
            out.write('\t\t\telsif bf = any ')
            out.write("(('{")
            for ind, g in enumerate(second):
                out.write(g + (", " if ind != len(second) - 1 else ''))
            out.write("}')::text[]) then\n")
            out.write('\t\t\t\tupdate "Spacers" set "BacStammId" = ' + str(num + 2) + ' where "Id" = i;\n')
            out.write('\t\tend if;\n\t\tend loop;\n') # сформирован код на основе данных
            first = set()
            second = set()
            third = set()
            num += 2
        name = x
    else:
        bf = x.split()
        if len(bf) == 4:
            dr.add(bf[1])
            mass.add(bf[2])


out.write('\tend;\n$$ LANGUAGE plpgsql;')
