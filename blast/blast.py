
from Bio.Blast import NCBIWWW, NCBIXML
from Bio import Entrez
from Bio import SeqIO


inp = open('input.txt', 'r')
out = open('blast_out.txt', 'a')
Entrez.email = "den.den073@yandex.ru"
complemetar = {"G": "C", "T": "A", "C": "G", "A": "T"}
for k in inp: # для всех бактерий
    x = k.strip()
    name = x.split(';')[0]
    mass = x.split(';')[1:]
    out.write(name + '\n')
    for g in mass: # для всех спейсеров
        out.write('\t' + g + '\n')
        try:
            result_handle = NCBIWWW.qblast("blastn", "nt", g) # бласт
            parser = NCBIXML.read(result_handle)
        except Exception:
            continue
        pam = 10
        for i in parser.alignments: # для всех результатов
            try:
                handle = Entrez.efetch(db="nucleotide", id=i.title.split('|')[1], rettype="gb", retmode='text') # поиск по gi
                bf = list(SeqIO.parse(handle, "gb"))
                #print('.......')
                if "taxonomy" in bf[0].annotations and 'Viruses' in bf[0].annotations["taxonomy"]:
                    for hsp in i.hsps:
                        seq = bf[0].seq
                        out.write('\t\t' + hsp.sbjct + ';')
                        left = ''.join(seq[hsp.sbjct_start - len(hsp.sbjct): hsp.sbjct_start])
                        right = ''.join((seq[hsp.sbjct_start - 1:hsp.sbjct_start + len(hsp.sbjct) - 1]))
                        need = ''.join([complemetar[m] for m in hsp.sbjct][::-1]) 
                        needright = hsp.sbjct # определение направления
                        out.write(i.title.split('|')[-1] + ';')
                        start = hsp.sbjct_start
                        if left == need:
                            end = start - len(hsp.sbjct)
                            a, b = ''.join(seq[end - pam: end]), ''.join(seq[start: start + pam])
                            out.write(a + b + '\n')
                        elif right == needright:
                            start = start - 1
                            end = start + len(hsp.sbjct)
                            a, b, = ''.join([complemetar[m] for m in seq[start - pam:start]][::-1]), ''.join([complemetar[m] for m in seq[end: end + pam]][::-1])
                            out.write(b + a + '\n') # определение PAM 
                        else:
                            out.write('oof\n')
                handle.close()
            except Exception:
                handle.close()
                continue
            out.close()
            out = open('blast_out.txt', 'a') # запись
out.close()


            #print(i.title, hsp.sbjct_start)
                  #bf[0].seq[
                                     #:hsp.sbjct_start + len(inpt)])
    #print('.......')


#handle = Entrez.efetch(db="nucleotide", id="1608083911", rettype="gb", retmode='text')
#print(list(SeqIO.parse(handle, "gb"))[0].seq)
##res = \
##for i in
##    print(i)
##print(Entrez.read(res))
