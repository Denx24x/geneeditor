inpt = ['S1_R1', 'S1_R2', 'S4_R1', 'S4_R2', 'S5_R1', 'S5_R2', 'S6_R1', 'S6_R2']
for i in inpt:
    mass = dict()
    inp = open(i + '.fastq', 'r')
    out = open(i + '.txt', 'w')
    for g in inp:
        x = g.strip()
        if x[0] != '+' and x[0] != 'я' and x[0] != '@':
            if x not in mass:
                mass[x] = 0
            mass[x] += 1
    res = sorted(mass.items(), key=lambda b:b[1], reverse=True)
    for i in range(len(res)):
        if i != 0:
            for g in range(min(len(res[i][0]), len(res[i - 1][0]))):
                out.write('|' if res[i][0][g] == res[i - 1][0][g] else ' ')
            out.write('\n')
        out.write(res[i][0] + ' - ' + str(res[i][1]) + '\n')
    out.close()
